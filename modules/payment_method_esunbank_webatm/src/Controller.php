<?php

namespace Drupal\payment_method_esunbank\Webatm;

use Drupal\payment_gateway\Controller as Base;

//use Drupal\payment_method_esunbank\Controller as Base;

class Controller extends Base {

  function __construct() {
    parent::__construct();
    $this->description = t('');
    $this->title = t('eSunBank WebATM');
    $default = &$this->controller_data_defaults;
    $default['virtual_account'] = '';
  }

}
