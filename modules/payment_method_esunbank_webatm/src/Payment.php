<?php

namespace Drupal\payment_method_esunbank\Webatm;

use Drupal\payment_gateway\Payment as Base;

class Payment extends Base {

  function invokeKey(array $data) {
    $list = array(
      $data['IcpNo'],
      $data['VAccNo'],
      $data['IcpConfirmTransURL'],
      $data['TransNo'],
      $data['TransAmt'],
      $this->data['merchant_key'],
    );

    return strtoupper(sha1(implode('', $list)));
  }

  function invokeData() {
    $controller = &$this->data;
    $context = &$this->check->payment->context_data;

    $data = array(
      'IcpNo' => $controller['merchant_code'],
      'VAccNo' => $controller['virtual_account'],
      'IcpConfirmTransURL' => $this->returnURL(),
      'TransNo' => $context['orderNumber'],
      'TransAmt' => strval($context['total']['amount']),
      'TransDesc' => 'eSunBank WebATM',
      'StoreName' => 'eSunBank WebATM',
    );

    $data['TransIdentifyNo'] = $this->invokeKey($data);
    return $data;
  }

  function returnKey(array $data) {
    $list = array(
      $data['IcpNo'],
      $data['TransNo'],
      $data['TransAmt'],
      $data['atmTradeNo'],
      $data['atmTradeDate'],
      $this->data['merchant_key'],
      $data['atmTradeState'],
    );

    return strtoupper(sha1(implode('', $list)));
  }

  function returnCheck() {
    $core = &$this->core;
    $data = &$this->payment->context_data[$this->name]['return']['data'];
    $list = array('IcpNo', 'TransNo', 'TransAmt', 'atmTradeNo', 'atmTradeDate', 'atmTradeState');

    if (FALSE == PaymentGateway::arrayAllKeyExist($data, $list)) {
      $this->finishStatus = PAYMENT_STATUS_FAILED;
      $core->setError(t('Transaction Data is invalidated.'));
      return;
    }


    if ('S' != $data['atmTradeState']) {
      if ('Z' == $data['atmTradeState']) {
        $this->finishStatus = PAYMENT_STATUS_PENDING;
      }

      if ('F' == $data['atmTradeState']) {
        if (in_array($data['atmErrNo'], array('0002', '0003'))) {
          $this->finishStatus = PAYMENT_STATUS_PENDING;
        }
      }

      $core->setError(t('Transaction is failure.'));
      return;
    }

    $controller = &$this->data;

    if ($controller['merchant_code'] != $data['IcpNo']) {
      $core->setError(t('Merchant Code is invalidated.'));
    }

    if ($this->returnKey($data) != $data['atmIdentifyNo_New']) {
      $core->setError(t('Check Key is invalidated.'));
    }

    $this->finishStatus = ($core->hasError()) ? PAYMENT_STATUS_FAILED : PAYMENT_STATUS_SUCCESS;
  }

}
