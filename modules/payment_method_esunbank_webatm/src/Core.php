<?php

namespace Drupal\payment_method_esunbank\Webatm;

use Drupal\payment_gateway\Core as Base;

//use Drupal\payment_method_esunbank\Core as Base;

class Core extends Base {

  function initAction() {
    $list = parent::initAction();
    $payment = &$list['payment'];
    $payment['returnOrderNumberKey'] = 'TransNo';
    return $list;
  }

  function schemaBody() {
    $data = parent::schemaBody();
    $field = &$data['fields'];

    $field['virtual_account'] = array(
      'type' => 'varchar',
      'length' => 16,
      'not null' => TRUE,
      'default' => '',
    );

    return $data;
  }

  function methodSetting(array $element, array &$form_state) {
    $form = parent::methodSetting($element, $form_state);
    $data = PaymentGateway::controllerData($form_state['payment_method']);
    $basic = &$form['Basic'];

    $basic['virtual_account'] = array(
      '#type' => 'textfield',
      '#title' => t('Virtual Account'),
      '#default_value' => $data['virtual_account'],
      '#maxlength' => 16,
    );

    return $form;
  }

}
