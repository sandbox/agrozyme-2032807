<?php

namespace Drupal\payment_method_esunbank\Webatm;

use Drupal\payment_gateway\Query as Base;

class Query extends Base {

  function invokeData() {
    $controller = &$this->data;
    $context = &$this->payment->context_data;

    $data = array(
      'IcpNo' => $controller['merchant_code'],
      'TransNo' => $context['orderNumber'],
    );

    return $this->sendRequestData($data, TRUE);
  }

  function returnKey(array $data) {
    $list = array(
      $data['merchant_code'],
      $data['order_number'],
      $data['amount'],
      $data['transcation_number'],
      $data['date_time'],
      $this->data['merchant_key'],
    );

    return strtoupper(sha1(implode('', $list)));
  }

  function returnData() {
    $list = explode('|', $this->payment->context_data[$this->name]['return']['raw']->data);
    $data = array(
      'merchant_code' => '',
      'order_number' => '',
      'amount' => '',
      'transcation_number' => '',
      'date_time' => '',
      'status' => '',
      'error_code' => '',
      'error_description' => '',
      'key' => '',
      'fee' => '',
      'transfer_in_bank_code' => '',
      'account' => '',
      'transfer_out_bank_code' => '',
    );

    foreach ($data as $key => $value) {
      $data[$key] = strval(array_shift($list));
    }

    return $data;
  }

  function returnCheck() {
    $core = &$this->core;
    $controller = &$this->data;
    $data = &$payment->context_data[$this->name]['return']['data'];

    switch ($data['status']) {
      case 'S':
        $this->finishStatus = PAYMENT_STATUS_SUCCESS;
        break;
      case 'F':
        $this->finishStatus = PAYMENT_STATUS_FAILED;
        $core->setError(t('Transaction is failure.'));
        break;
    }

    if ($data['key'] != $this->returnKey($data)) {
      $this->finishStatus = PAYMENT_STATUS_FAILED;
      $core->setError(t('Check Key is invalidated.'));
    }
  }

}
