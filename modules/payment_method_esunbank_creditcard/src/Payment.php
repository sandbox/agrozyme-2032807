<?php

namespace Drupal\payment_method_esunbank_creditcard;

use Drupal\payment_gateway\Payment as Base;

class Payment extends Base {

  function invokeKey(array $data) {
    $data = array(
      $data['MID'],
      $data['CID'],
      $data['TID'],
      $data['ONO'],
      $data['TA'],
      $data['U'],
      $this->data['merchant_key'],
    );

    dpm($data);

    return md5(implode('&', $data));
  }

  function invokeData() {
    $controller = &$this->data;
    $context = &$this->check->payment->context_data;
    dpm($controller);
    dpm($context);

    $data = array(
      'MID' => $controller['merchant_code'],
      'CID' => $controller['counter_code'],
      'TID' => (1 >= intval($context['Setting']['installment'])) ? 'EC000001' : 'EC000002',
      'ONO' => $context['orderNumber'],
      'TA' => strval($context['price']['amount']),
      'U' => $this->returnURL(),
    );

    $data['M'] = $this->invokeKey($data);

    dpm($data);
    return $data;
  }

  function returnKey(array $list) {
    $data = array(
      $list['RC'],
      $list['MID'],
      $list['ONO'],
      $list['LTD'],
      $list['LTT'],
      $list['RRN'],
      $list['AIR'],
      $list['AN'],
      $this->data['merchant_key'],
    );

    return md5(implode('&', $data));
  }

  function returnCheck() {
    if (FALSE == parent::returnCheck()) {
      return FALSE;
    }

    $core = $this->core;
    $data = (array) $core->payment->context_data[$this->name]['return']['data'];

    if ('00' != $data['RC']) {
      $core->setError(t('Transaction is failure.'));
    }

    if ($this->data['merchant_code'] != $data['MID']) {
      $core->setError(t('Merchant Code is invalidated.'));
    }

    if ($this->returnKey($data) != $data['M']) {
      $core->setError(t('Check Key is invalidated.'));
    }

    if ($core->hasError()) {
      return FALSE;
    } else {
      $this->finishStatus = PAYMENT_STATUS_ACCEPTED;
      return TRUE;
    }
  }

}
