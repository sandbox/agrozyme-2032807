<?php

namespace Drupal\payment_method_esunbank_creditcard;

use Drupal\payment_gateway\Controller as Base;

//use Drupal\payment_method_esunbank\Controller as Base;

class Controller extends Base {

  function __construct() {
    parent::__construct();
    $this->description = t('');
    $this->title = t('eSunBank Credit Card');
    $default = &$this->controller_data_defaults;
    $default['firm_code'] = '';
    $default['counter_code'] = '';
  }

}
