<?php

namespace Drupal\payment_method_esunbank_creditcard;

use Drupal\payment_gateway\Core as Base;

//use Drupal\payment_method_esunbank\Core as Base;

class Core extends Base {

  function initAction() {
    $data = parent::initAction();
    $payment = &$data['payment'];
    $payment['returnOrderNumberKey'] = 'ONO';
    $payment['returnSuccessKeyList'] = array('RC', 'MID', 'ONO', 'LTD', 'LTT', 'RRN', 'AIR', 'AN', 'M');
    $payment['returnFailureKeyList'] = array('RC', 'MID', 'ONO');

    $query = &$data['query'];
    $query['returnSuccessKeyList'] = array('RC', 'MID', 'ONO');
    $query['returnFailureKeyList'] = array('RC', 'MID', 'ONO');
    return $data;
  }

  function schemaBody() {
    $data = parent::schemaBody();
    $field = &$data['fields'];

    $field['counter_code'] = array(
      'type' => 'varchar',
      'length' => 20,
      'not null' => TRUE,
    );

    $field['firm_code'] = array(
      'type' => 'varchar',
      'length' => 4,
      'not null' => TRUE,
    );

    return $data;
  }

  function installmentOption() {
    $data = array(1, 3, 6, 12);
    return array_combine($data, $data);
  }

  function paymentSetting(array $element, array &$form_state) {
    $form = parent::paymentSetting($element, $form_state);
    $data = &$form_state['payment']->context_data['Setting'];

    if (empty($data['installment'])) {
      $data['installment'] = 1;
    }

    $setting = &$form['Setting'];
    $setting['installment'] = array(
      '#type' => 'select',
      '#title' => t('Installment'),
      '#options' => $this->installmentOption(),
      '#default_value' => $data['installment'],
      '#required' => TRUE,
    );

    return $form;
  }

  public function methodSettingBasicFieldset(array $data) {
    $list = parent::methodSettingBasicFieldset($data);
    $default = $data['default'];

    $list['counter_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Counter Code'),
      '#default_value' => $default['counter_code'],
      '#maxlength' => 20,
    );

    $list['firm_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Firm Code'),
      '#default_value' => $default['firm_code'],
      '#maxlength' => 4,
      '#required' => FALSE,
    );

    return $list;
  }

}
