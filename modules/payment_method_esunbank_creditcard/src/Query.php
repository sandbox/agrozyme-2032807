<?php

namespace Drupal\payment_method_esunbank_creditcard;

use Drupal\payment_gateway\Payment as Base;

class Query extends Base {

  function invokeKey(array $list) {
    $data = array(
      $list['MID'],
      $list['ONO'],
      $this->data['merchant_key'],
    );

    return md5(implode('&', $data));
  }

  function invokeData() {
    $controller = &$this->data;
    $context = &$this->payment->context_data;

    $list = array(
      'MID' => $controller['merchant_code'],
      'ONO' => $context['orderNumber'],
    );

    $list['M'] = $this->snedKey($list);
    return $this->sendRequestData($list);
  }

  function returnData() {
    $list = array();
    parse_str(str_replace(',', '&', $this->payment->context_data[$this->name]['return']['raw']['data']), $list);
    return $list;
  }

  function returnCheck() {
    if (FALSE == parent::returnCheck()) {
      return FALSE;
    }

    $core = $this->core;
    $action = $this->name;
    $data = &$this->payment->context_data[$action]['return']['data'];

    $list = array(
      '00' => PAYMENT_STATUS_ACCEPTED,
      '10' => PAYMENT_STATUS_ACCEPTED,
      '11' => PAYMENT_STATUS_AUTHORIZATION_FAILED,
      '10' => PAYMENT_STATUS_PENDING,
      '59' => PAYMENT_STATUS_SUCCESS,
    );

    if (isset($list[$data['RC']])) {
//			$this->finishStatus = $table[$data['RC']];
    }
  }

}
