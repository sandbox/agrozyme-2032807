<?php

namespace Drupal\payment_method_esunbank;

use Drupal\payment_gateway\Core as Base;

abstract class Core extends Base {

  function paymentCurrencyOption() {
    $index = 'TWD';
    $list = parent::paymentCurrencyOption();
    return array($index => $list[$index]);
  }

  function schemaBody() {
    $data = parent::schemaBody();
    $field = &$data['fields'];
    $field['merchant_code']['length'] = 15;
    $field['merchant_key']['length'] = 32;
    return $data;
  }

}
