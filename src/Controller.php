<?php

namespace Drupal\payment_method_esunbank;

use Drupal\payment_gateway\Controller as Base;

abstract class Controller extends Base {

  function __construct() {
    parent::__construct();
    $default = &$this->controller_data_defaults;
    $default['payment_currency'] = 'TWD';
    $default['payment_decimal'] = 0;
    $default['display_decimal'] = 0;
  }

}
